/* === On DOM load === */

if(document.readyState === 'complete') {
  init();
} else {
  document.addEventListener('DOMContentLoaded', function () {
    init();
  });
}

// Init function
function init() {
  Images.init();
}


/* === Images functions=== */

Images = {
  
  // Adding the nav event listener
  init: function() {

    let placeholder = document.querySelectorAll('div[data-js-replace]');

    for(let i = 0; i < placeholder.length; i++) {
      Images.loadOriginal(placeholder[i]);
    }

  },

  // Loading the original images
  loadOriginal: function(placeholder) {

    let original = placeholder.getAttribute('data-js-replace'),
        img = document.createElement('img'),
        innerContainer = placeholder.querySelector('.img_inner_container');

    img.src = '/img/' + original;

    img.onload = function() {
      empty(innerContainer);
      innerContainer.appendChild(img);

      Images.toggleBlur(innerContainer);
    }

  },

  // Toggling the blur
  toggleBlur: function(elm) {
    elm.classList.toggle('blur');
  }

}


/* === Utility functions=== */

// Clearing a container
function empty(elm) {
  while(elm.firstChild){
    elm.removeChild(elm.firstChild);
  }
}