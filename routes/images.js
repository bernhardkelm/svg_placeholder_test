/* === Extensions import === */

const util = require('./util.js'),
      uuid = require('node-uuid'),
      formidable = require('formidable');


/* === Image actions === */

// Image upload
exports.upload = function(db, imagesModel) {
  return function(req, res) {
    // Grabbing the multipart formdata via formidable
    let form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
      if(err) throw err;

      // Getting the file extension and generating a name
      let typeArray = files.image.type.split('/'),
          uuid_name = uuid.v1(),
          generatedName = uuid_name + '.' + typeArray[1],
          path = './public/img/' + generatedName;

      // Saving the image locally
      util.upload_and_convert(path, files.image.path, uuid_name, function(placeholder) {

        // Creating the db entry
        let newImage = imagesModel({
          image: generatedName,
          placeholder: placeholder
        });

        // Writing the new db entry
        newImage.save(function(err) {
          if(err) throw err;
          res.redirect(req.get('referer'));
        });
      });
    });
  }
}
