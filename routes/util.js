/* === Extensions import === */

var config = require('../config.json'),
    sizeOf = require('image-size'),
    fs = require('fs');


/* === Utility functions === */

// Saving an image
exports.upload_and_convert = function(path, image, name, cb) {
  let rstream = fs.createReadStream(image),
      wstream = fs.createWriteStream(path);
  rstream.pipe(wstream);
  rstream.on('end', function(){
    // ToDo: Get image dimensions (1)
    generatePlaceholder(path, name, function(placeholder) {
      cb(placeholder);
    });
  });
};

// ToDo: Replace svg tag <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 1000 625"> (2)
// Generating a placeholder SVG
function generatePlaceholder(path, name, cb) {
  // Running the image converion using primitive
  let exec = require('child_process').exec;
  exec('primitive -i ' + path + ' -o ./public/img/' + name + '.svg -n ' + config.imageVectors, (err, stdout, stderr) => {
    if(err) throw err;

    let placeholder = fs.readFileSync('./public/img/' + name + '.svg','utf8');

    // Getting the images dimensions
    let dimensions = sizeOf(path),
        ratio = (dimensions.height / dimensions.width) * 1000;

    //'<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 1000 ' + ratio + '">'
    placeholder = placeholder.replace(/[^>]*/, '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 1000 ' + ratio + '"');

    fs.unlink('./public/img/' + name + '.svg', function() {
      cb(placeholder);
    });
  });
};
