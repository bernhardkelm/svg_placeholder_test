/* === Extensions import === */

const config      = require('./config.json'),
      http        = require('http'),
      express     = require('express'),
      ejs         = require('ejs'),
      mongoose    = require('mongoose'),
      bodyParser  = require('body-parser'),
      jsonParser  = bodyParser.json(),
      cluster     = require('cluster'),
      app         = express();
  

/* === express configuration === */

app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + '/public'));


/* === Database configuration === */

const mongooseOptions = {'useMongoClient': true}
mongoose.connect('mongodb://localhost/' + config.db, mongooseOptions);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('Connected to DB');
});


/* === Defining Database models === */

const imagesModel = require('./models/imagesModel');


/* === Setting up Routes === */

const images = require('./routes/images');


/* === Public Routes === */

// GET - Frontpage
app.get('/', function(req, res) {
  imagesModel.find({}, function(err, images) {
    if(err) throw err;
    res.render('index.ejs', {images: images});
  });
});

// GET - SVG Demo
app.get('/svg_demo', function(req, res) {
  imagesModel.find({}, function(err, images) {
    if(err) throw err;
    res.render('svg_demo.ejs', {images: images});
  });
});

// GET - Img Demo
app.get('/img_demo', function(req, res) {
  imagesModel.find({}, function(err, images) {
    if(err) throw err;
    res.render('img_demo.ejs', {images: images});
  });
});

// GET - Lazy Demo
app.get('/lazy_demo', function(req, res) {
  imagesModel.find({}, function(err, images) {
    if(err) throw err;
    res.render('lazy_demo.ejs', {images: images});
  });
});

// POST - Image
app.post('/image', images.upload(db, imagesModel));


/* === 404 Route === */

//404 Route, ALWAYS KEEP AT THE BOTTOM
app.get('*', function(req, res){
  res.status(404).send('This is not the page you are looking for. You can get back <a href="/">here</a>');
});


/* === Server startup === */

//Uncaught error handling
let workers = process.env.WORKERS || require('os').cpus().length;
if(workers > 4) workers = config.workers || 2;

if(cluster.isMaster) {
  console.log('start cluster with %s workers', workers);
  for(let i = 0; i < workers; ++i) {
    let worker = cluster.fork().process;
    console.log('worker %s started.', worker.pid);
  }
  cluster.on('exit', function(worker) {
    console.log('worker %s died. restart...', worker.process.pid);
    cluster.fork();
  });
} else {
  let httpsserver = http.createServer(app).listen(config.port, config.host);
}

process.on('uncaughtException', function (err) {
  console.error((new Date).toUTCString() + ' uncaughtException:', err.message)
  console.error(err.stack)
  process.exit(1)
});
