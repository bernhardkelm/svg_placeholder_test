//Dependencies
const mongoose = require('mongoose'),
      Schema = mongoose.Schema;

//Creating the user schema
let imagesSchema = new Schema({
  image: { type: String },
  placeholder: { type: String }
});

let images = mongoose.model('images', imagesSchema);

module.exports = images;
